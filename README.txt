DESCRIPTION
-----------
Repairing ajax form submit process.
When user tries to submit ajax form loaded in browser some time ago
(yesterday, etc.) and cache has been already deleted
on backend - user will get error 'Invalid form POST data.'
because 'form_build_id' from his old markup can't pass validation.

Issue description: https://www.drupal.org/node/1694574

INSTALLATION
------------
Just activate module as usually.

There is an allow list that can call a function that must return an array with
the #build_id key set if drupal_rebuild_form() does not work for the form.
